//
//  ContactTableViewCell+ConfigureCell.h
//  Simpletechs
//
//  Created by A. J. on 29/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ContactTableViewCell.h"

@interface ContactTableViewCell (ConfigureCell)

- (void)configureCellWithContact:(Contact*)contact;

@end
