//
//  ContactTableViewCell+ConfigureCell.m
//  Simpletechs
//
//  Created by A. J. on 29/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "ContactTableViewCell+ConfigureCell.h"

@implementation ContactTableViewCell (ConfigureCell)

- (void)configureCellWithContact:(Contact*)contact
{
    self.name.text = contact.name;
    self.email.text = contact.email;
}

@end
