//
//  MainTableViewController.m
//  Simpletechs
//
//  Created by A. J. on 29/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "MainTableViewController.h"
#import "Network+Contact.h"
#import "ContactTableViewCell+ConfigureCell.h"

@interface MainTableViewController ()<UITextFieldDelegate>

@property (nonatomic, strong)NSMutableArray *contacts;
@property (weak, nonatomic) IBOutlet UITextField *emailNewContact;
@property (weak, nonatomic) IBOutlet UITextField *nameNewContact;
@property (weak, nonatomic) IBOutlet UIButton *saveNewContact;
@property (strong, nonatomic) NSString *emailToSave;
@property (strong, nonatomic) NSString *nameToSave;

@end

@implementation MainTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.contacts = [NSMutableArray arrayWithArray:[[[Network sharedManager] database] allContacts]];
    [self updateContactsWithCompletionHandler:^(BOOL finished) {
        if(finished){
            [self updateTableview];
        }
    }];
     
}

#pragma mark - Custom Methods
- (void)updateTableview
{
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name"
                                                 ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [self.contacts sortedArrayUsingDescriptors:sortDescriptors];
    self.contacts = [NSMutableArray arrayWithArray:sortedArray];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

- (void)updateContactsWithCompletionHandler:(GetNewContacts)completion
{
    [[Network sharedManager] getUpdateContactsWithCompletionHandler:^(NSArray *response, NSError *error) {
        if(response.count)
        {
            self.contacts = [NSMutableArray arrayWithArray:[[[Network sharedManager] database] allContacts]];
            
        }
        completion(YES);
    }];

}

- (void)clearRegistrationFields
{
    self.nameToSave = self.nameNewContact.text;
    self.emailToSave = self.emailNewContact.text;
    self.nameNewContact.text = @"";
    self.emailNewContact.text = @"";
}

- (BOOL)isAValidContact
{
    BOOL isAValidContact = self.nameNewContact.text.length>0 && self.emailNewContact.text.length>0 ? YES : NO;
    
    if(!isAValidContact)
    {
        [self showAlertWithTitle:@"Attention" andText:@"You need to complete the registration"];
    }else
    {
        [self clearRegistrationFields];
    }
    return isAValidContact;
}

#pragma mark - Custom Alert
- (void)showAlertWithTitle:(NSString *)title andText:(NSString *)text
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:text preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    [self presentViewController:alertController animated:YES completion:nil];
}

#pragma mark - TableViewDataSource
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        Contact *c = self.contacts[indexPath.row];
        [[[Network sharedManager] database] removeRecord:c];
        [[[Network sharedManager] database] save];
        [self.contacts removeObjectAtIndex:indexPath.row];
        [tableView reloadData];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.contacts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ContactTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ContactTableViewCellId"forIndexPath:indexPath];
    if (cell == nil) {
        cell = [ContactTableViewCell new];
    }
    Contact *contact = [self.contacts objectAtIndex:indexPath.row];
    [cell configureCellWithContact:contact ];
    return cell;
}

#pragma mark - TextFieldDelegate
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    self.saveNewContact.enabled = NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    if (textField == self.emailNewContact) {
        [self.nameNewContact becomeFirstResponder];
    } else
    {
        self.saveNewContact.enabled = YES;
    }
    return NO;
}

#pragma mark - Actions
- (IBAction)saveNewContactPressed:(id)sender
{
    if([self isAValidContact]){
        Contact *contact = [[[Network sharedManager] database] newContact];
        [contact startContactWithName:self.nameToSave andEmail:self.emailToSave];
        [[[Network sharedManager] database] save];
        [self updateContactsWithCompletionHandler:^(BOOL finished) {
            if(finished){
                [self updateTableview];
            }
        }];
    }
    
}

- (IBAction)doRefresh:(id)sender
{
    [self updateContactsWithCompletionHandler:^(BOOL finished) {
        if(finished){
            [self updateTableview];
            [sender endRefreshing];
        }
    }];
}

#pragma mark - MemoryWarning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
