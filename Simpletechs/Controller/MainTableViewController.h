//
//  MainTableViewController.h
//  Simpletechs
//
//  Created by A. J. on 29/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainTableViewController : UITableViewController

typedef void (^GetNewContacts)(BOOL finished);

@end
