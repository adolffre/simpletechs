//
//  ContactTableViewCell.h
//  Simpletechs
//
//  Created by A. J. on 29/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Contact.h"

@interface ContactTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *email;

@end
