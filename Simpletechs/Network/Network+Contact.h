//
//  Network+Contact.h
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Network.h"
#import <Foundation/Foundation.h>

@interface Network (Contact)
/**
 *  Get the updated contacts
 *
 *  @param completion as above
 */
- (void)getUpdateContactsWithCompletionHandler:(GetContacts)completion;

@end
