//
//  Network+Contact.m
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Network+Contact.h"
#import "Contact.h"

@implementation Network (Contact)

#pragma mark - Requests

- (void)getUpdateContactsWithCompletionHandler:(GetContacts)completion
{
    NSString *requestUrl = self.apiUrl;
    // Request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request setURL:[NSURL URLWithString:requestUrl]];
    [[Network sharedManager] getDataUsingRequest:request withCompletionHandler:^(NSArray *response , NSError *error) {
        if(error)
        {
            completion(nil, error);
        }else
        {
            Contact *contact;
            for (NSDictionary *contactDictionary in response)
            {
                NSString *idCode = [NSString stringWithFormat:@"%@",contactDictionary[@"id"]];
                if(idCode)
                {
                    contact = [[[Network sharedManager] database] contactWithIdCode:idCode];
                    if(!contact)
                    {
                        contact = [[[Network sharedManager] database] newContact];
                    }
                    
                    [contact startContactWithDict:contactDictionary];
                }
            }
            [[[Network sharedManager] database] save];
            completion(response,nil);
        }

    }];
    
}

@end
