//
//  Network.m
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Network.h"

@implementation Network

#pragma mark - Singleton Methods
+ (instancetype)sharedManager
{
    static Network *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
        sharedMyManager.apiUrl = @"http://www.filltext.com/?rows=10&name=%7BfirstName%7D~%7BlastName%7D&mail=%7Bemail%7D&id=%7Bindex%7D&pretty=true";
        sharedMyManager.database = [DatabaseManager sharedManager];
    });
    return sharedMyManager;
}

#pragma mark - Requests
- (void)getDataUsingRequest:(NSMutableURLRequest *)request withCompletionHandler:(GetContacts)completion
{
    // Execute Request
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSHTTPURLResponse *httpResponse =(NSHTTPURLResponse *)response;
        if(error || httpResponse.statusCode !=200){
            completion(nil,error);
        }else{
            NSError *err = nil;
            NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData:data
                                                                           options:NSJSONReadingMutableContainers
                                                                             error:&err];
            completion(jsonArray,nil);
        }
    }];
    [task resume];
    
}

#pragma mark - error
- (NSString *)showErrorMessage:(NSError *)error
{
    return error.localizedDescription;
}
@end
