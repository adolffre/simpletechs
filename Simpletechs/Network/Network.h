//
//  Network.h
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DatabaseManager.h"

@interface Network : NSObject
/**
 *  GetContacts completion
 *
 *  @param success           Response array
 *  @param error             Error for failure
 */
typedef void (^GetContacts)(NSArray *response, NSError *error);

#pragma mark - Properties
/**
 *  Api url
 */
@property (nonatomic) NSString *apiUrl;
/**
 *  Database
 */
@property (nonatomic) DatabaseManager *database;

#pragma mark - Singleton

+ (instancetype)sharedManager;

#pragma mark - Request
/**
 *  Request
 *
 *  @param completion as above
 */
- (void)getDataUsingRequest:(NSMutableURLRequest *)request withCompletionHandler:(GetContacts)completion;
/**
 *  Return string error from error
 *
 *  @param error        Error for failure
 */
- (NSString *)showErrorMessage:(NSError *)error;
@end
