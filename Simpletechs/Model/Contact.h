//
//  Contact.h
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Contact : NSManagedObject

- (void)startContactWithDict:(NSDictionary *)dictionary;

- (void)startContactWithName:(NSString *)name andEmail:(NSString *)email;
@end

NS_ASSUME_NONNULL_END

#import "Contact+CoreDataProperties.h"
