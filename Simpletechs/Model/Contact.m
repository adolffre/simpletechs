//
//  Contact.m
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import "Contact.h"

@implementation Contact

- (void)startContactWithDict:(NSDictionary *)dictionary
{
    NSString *idCode = [NSString stringWithFormat:@"%@",dictionary[@"id"]];
    self.idCode = idCode;
    self.name = dictionary[@"name"];
    self.email = dictionary[@"mail"];
}
- (void)startContactWithName:(NSString *)name andEmail:(NSString *)email
{
    
    NSTimeInterval today = [[NSDate date] timeIntervalSince1970];
    NSString *idCode = [NSString stringWithFormat:@"%f", today];
    self.name = (NSString *)name;
    self.email = (NSString *)email;
    self.idCode = idCode;
}
@end
