//
//  DatabaseManager.h
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Contact+CoreDataProperties.h"

@interface DatabaseManager : NSObject

#pragma mark - Singleton

+ (instancetype)sharedManager;

#pragma mark - Properties

@property (nonatomic) NSManagedObjectContext *context;

#pragma mark - Create

- (Contact *)newContact;

#pragma mark - Read

- (NSArray *)allContacts;
- (Contact *)contactWithIdCode:(NSString *)idCode;

#pragma mark - Update

- (BOOL)save;

#pragma mark - Delete

- (BOOL)removeRecord:(NSManagedObject *)record;


@end
