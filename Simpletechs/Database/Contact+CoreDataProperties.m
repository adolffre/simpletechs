//
//  Contact+CoreDataProperties.m
//  Simpletechs
//
//  Created by A. J. on 28/07/16.
//  Copyright © 2016 AJ. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "Contact+CoreDataProperties.h"

@implementation Contact (CoreDataProperties)

@dynamic idCode;
@dynamic name;
@dynamic email;

@end
